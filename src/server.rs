//! Control Webserver

use std::net::{IpAddr, SocketAddr};
use console::style;
use warp::Filter;

use super::settings::Settings;

/// Start webserver
pub async fn start(settings: Settings) {        
    let quiet = settings.quiet;
    let ip: Result<IpAddr, _> = settings.server.ip.parse();
    let port = settings.server.port;
    let webroot = settings.server.webroot;

    let files = warp::fs::dir(webroot.clone());
    let routes = files;

    match ip {
        Ok(ip) => {
            // output serving information
            if !quiet {
                println!(
                    "{}",
                    style(format!(
                        "Server started on http://{}:{}",
                        ip, port
                    ))
                    .bold()
                    .green()
                );
                println!("{}", style(format!("Serving {}", &webroot)));
            }
            // start server
            let socket_adr: SocketAddr = (ip, port).into();
            warp::serve(routes.map(|file| {
                println!("{:?}", file);
                file
            }))
            .run(socket_adr)
            .await;
        }
        Err(e) => {
            println!(
                "{}",
                style(format!("Server could not start: {}", e)).bold().red()
            );
        }
    }
}
