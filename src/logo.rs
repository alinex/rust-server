//! Output colorized ASCII Art Logo
//!
//! Use `print()` function to output the logo with a custom title.

use console::style;
use std::env;

/// Call this function with a title to print a graphical logo:
///
/// ```
/// use crate::logo;
/// logo::print("Basic Webserver");
///
/// //                           __   ____     __
/// //           ######  #####  |  | |    \   |  |   ########### #####       #####
/// //          ######## #####  |  | |     \  |  |  ############  #####     #####
/// //         ######### #####  |  | |  |\  \ |  |  #####          #####   #####
/// //        ########## #####  |  | |  | \  \|  |  #####           ##### #####
/// //       ##### ##### #####  |  | |  |_ \     |  ############     #########
/// //      #####  ##### #####  |  | |    \ \    |  ############     #########
/// //     #####   ##### #####  |__| |_____\ \___|  #####           ##### #####
/// //    #####    ##### #####                      #####          #####   #####
/// //   ##### ######### ########################## ############  #####     #####
/// //  ##### ##########  ########################   ########### #####       #####
/// // ____________________________________________________________________________
/// //
/// //                     B A S I C   W E B S E R V E R
/// // ____________________________________________________________________________
/// ```
///
/// Which logo to print can be defined by the environment variable `LOGO=alinex`.
pub fn print(title: &str) {
    // format title
    let title = &title
        .to_uppercase()
        .chars()
        .map(|c| c.to_string() + " ")
        .collect::<String>();
    // output logo
    let sign = env::var("LOGO").unwrap_or("alinex".to_string());
    match sign.as_ref() {
        "divibib" => divibib(title),
        "ekz" => ekz(title),
        _ => alinex(title),
    }
}

/// ASCII Art Logo for Alinex
fn alinex(title: &str) {
    println!();
    println!(
        "{}{}",
        style("                         ").cyan(),
        style(" __   ____     __").yellow().bold()
    );
    println!(
        "{}{}{}",
        style("          ######  #####  ").cyan(),
        style("|  | |    \\   |  |  ").yellow().bold(),
        style(" ########### #####       #####").cyan()
    );
    println!(
        "{}{}{}",
        style("         ######## #####  ").cyan(),
        style("|  | |     \\  |  |  ").yellow().bold(),
        style("############  #####     #####").cyan()
    );
    println!(
        "{}{}{}",
        style("        ######### #####  ").cyan(),
        style("|  | |  |\\  \\ |  |  ").yellow().bold(),
        style("#####          #####   #####").cyan()
    );
    println!(
        "{}{}{}",
        style("       ########## #####  ").cyan(),
        style("|  | |  | \\  \\|  |  ").yellow().bold(),
        style("#####           ##### #####").cyan()
    );
    println!(
        "{}{}{}",
        style("      ##### ##### #####  ").cyan(),
        style("|  | |  |_ \\     |  ").yellow().bold(),
        style("############     #########").cyan()
    );
    println!(
        "{}{}{}",
        style("     #####  ##### #####  ").cyan(),
        style("|  | |    \\ \\    |  ").yellow().bold(),
        style("############     #########").cyan()
    );
    println!(
        "{}{}{}",
        style("    #####   ##### #####  ").cyan(),
        style("|__| |_____\\ \\___|  ").yellow().bold(),
        style("#####           ##### #####").cyan()
    );
    println!(
        "{}",
        style("   #####    ##### #####                      #####          #####   #####").cyan()
    );
    println!(
        "{}",
        style("  ##### ######### ########################## ############  #####     #####").cyan()
    );
    println!(
        "{}",
        style(" ##### ##########  ########################   ########### #####       #####").cyan()
    );
    println!(
        "{}",
        style("____________________________________________________________________________")
            .cyan()
    );
    println!();
    println!("{: ^76}", style(title).yellow());
    println!(
        "{}",
        style("____________________________________________________________________________")
            .cyan()
    );
    println!();
}

/// ASCII Art Logo for divibib
fn divibib(title: &str) {
    println!();
    println!(
        "{}{}",
        style("               ###   #                     #   ")
            .black()
            .bold(),
        style("###           #   ###").green()
    );
    println!(
        "{}{}",
        style("               ###  ###                   ###  ")
            .black()
            .bold(),
        style("###          ###  ###").green()
    );
    println!(
        "{}{}",
        style("               ###   #                     #   ")
            .black()
            .bold(),
        style("###           #   ###").green()
    );
    println!(
        "{}{}",
        style("               ###                             ")
            .black()
            .bold(),
        style("###               ###").green()
    );
    println!(
        "{}{}{}",
        style(" ## ").green(),
        style("      ########  ###  ###         ###  ###  ")
            .black()
            .bold(),
        style("########     ###  ########").green()
    );
    println!(
        "{}{}{}",
        style("####").green(),
        style("    ###    ###  ###   ###       ###   ###  ")
            .black()
            .bold(),
        style("###    ###   ###  ###    ###").green()
    );
    println!(
        "{}{}{}",
        style(" ## ").green(),
        style("   ###     ###  ###    ###     ###    ###  ")
            .black()
            .bold(),
        style("###     ###  ###  ###     ###").green()
    );
    println!(
        "{}{}{}",
        style("    ").green(),
        style("   ###     ###  ###     ###   ###     ###  ")
            .black()
            .bold(),
        style("###     ###  ###  ###     ###").green()
    );
    println!(
        "{}{}{}",
        style(" ## ").green(),
        style("   ###     ###  ###      ### ###      ###  ")
            .black()
            .bold(),
        style("###     ###  ###  ###     ###").green()
    );
    println!(
        "{}{}{}",
        style("####").green(),
        style("    ###    ###  ###       #####       ###  ")
            .black()
            .bold(),
        style("###    ###   ###  ###    ###").green()
    );
    println!(
        "{}{}{}",
        style(" ## ").green(),
        style("      ########  ###        ###        ###  ")
            .black()
            .bold(),
        style("########     ###  ########").green()
    );
    println!(
        "{}",
        style("____________________________________________________________________________")
            .black()
            .bold()
    );
    println!();
    println!("{: ^76}", style(title).green());
    println!(
        "{}",
        style("____________________________________________________________________________")
            .black()
            .bold()
    );
    println!();
}

/// ASCII Art Logo for ekz
fn ekz(title: &str) {
    println!();
    println!(
        "{}",
        style("                                    ###                         ")
            .black()
            .bold()
    );
    println!(
        "{}",
        style("                                    ###                         ")
            .black()
            .bold()
    );
    println!(
        "{}",
        style("                                    ###                         ")
            .black()
            .bold()
    );
    println!(
        "{}",
        style("                                    ###                         ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("             ##  ").red(),
        style("      #######      ###      ###  ############# ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("            #### ").red(),
        style("    ###     ###    ###    ###             ###  ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("             ##  ").red(),
        style("   ###       ###   ###  ###             ###    ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("                 ").red(),
        style("  ###############  ######             ###      ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("             ##  ").red(),
        style("  ###              ###  ###         ###        ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("            #### ").red(),
        style("   ###             ###    ###     ###          ")
            .black()
            .bold()
    );
    println!(
        "{}{}",
        style("             ##  ").red(),
        style("     ##########    ###      ###  ############# ")
            .black()
            .bold()
    );
    println!(
        "{}",
        style("____________________________________________________________________________")
            .black()
            .bold()
    );
    println!();
    println!("{: ^76}", style(title).green());
    println!(
        "{}",
        style("____________________________________________________________________________")
            .black()
            .bold()
    );
    println!();
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
