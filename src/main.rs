//! Alinex Rust based Webserver

extern crate config;
extern crate serde;
#[macro_use]
extern crate serde_derive;
mod settings;
mod logo;

mod server;

use settings::Settings;

#[tokio::main]
async fn main() {
    let settings = Settings::new().ok().unwrap();

    // welcome
    if !settings.quiet {
        logo::print("Basic Web Server");
        println!("Version       {}", env!("CARGO_PKG_VERSION"));
        println!("Author        {}", env!("CARGO_PKG_AUTHORS"));
        println!();
    }
    
//    println!("{:?}", settings);

    // server start
    server::start(settings).await;
}
