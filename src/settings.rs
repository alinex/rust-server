//! Read configuration into structure.

use std::env;
use config::{ConfigError, Config, File, Environment};
use clap::{App, Arg};

use std::fs;
use std::path::PathBuf;

/// Subelement of Configuration
#[derive(Debug, Deserialize)]
pub struct Server {
    pub ip: String,
    pub port: u16,
    pub webroot: String,
}

/// Configuration Structure
#[derive(Debug, Deserialize)]
pub struct Settings {
    pub quiet: bool,
    pub server: Server,
}

impl Settings {
    /// Read configuration into struct.
    /// 
    /// Merging:
    /// - `default.*` file
    /// - `<profile>.*` file
    /// - `local.*` file
    /// - environment setting
    /// - CLI arguments
    /// 
    /// # Panics
    /// 
    /// - `server.webroot` directory not accessible
    pub fn new() -> Result<Self, ConfigError> {
        // read arguments
        let args = App::new(env!("CARGO_BIN_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .before_help("test")
        .arg(
            Arg::with_name("quiet")
            .short("q")
            .long("quiet")
            .global(true)
            .help("Output nothing unless really needed"),
        )
        .arg(
            Arg::with_name("config")
            .short("c")
            .long("config")
            .takes_value(true)
            .help("Config profile to use")
        )
        .arg(
            Arg::with_name("ip")
            .long("ip")
            .short("i")
            .takes_value(true)
            .help("Sets the address to use"),
        )
        .arg(
            Arg::with_name("port")
            .long("port")
            .short("p")
            .takes_value(true)
            .help("Sets the port."),
        )
        .arg(
            Arg::with_name("webroot")
            .long("webroot")
            .short("w")
            .help("The path to the files being served")
            .index(1),
        )
        .get_matches();

        // build setting
        let mut s = Config::default();
        
        // Start off by merging in the "default" configuration file
        s.merge(File::with_name("config/default"))?;

        // Find profile to use
        let profile = if args.is_present("config") {
            args.value_of("config").unwrap_or("development").to_owned()
        } else {
            env::var("ALINEX_CONFIG").unwrap_or_else(|_| "development".into())
        };
        // merge profile configuration
        s.merge(File::with_name(&format!("config/{}", profile)).required(false))?;

        // Add in a local configuration file
        // This file shouldn't be checked in to git
        s.merge(File::with_name("config/local").required(false))?;

        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `ALINEX_QUIET=1 alinex-server` would set the `quiet` key
        s.merge(Environment::with_prefix("alinex").separator("_"))?;

        // finally the arguments will overwrite anything
        if args.is_present("quiet") { s.set("quiet", true)?; }
        if args.is_present("ip") { s.set("server.ip", args.value_of("ip").unwrap())?; }
        if args.is_present("port") { s.set("server.port", args.value_of("port").unwrap())?; }
        if args.is_present("webroot") { s.set("server.webroot", args.value_of("webroot").unwrap())?; }

        // optimize values
        s.set("server.webroot", fs::canonicalize(PathBuf::from(s.get_str("server.webroot")?))
            .expect("Could not read webroot directory")
            .to_str().unwrap().to_owned())?;
    
        // You can deserialize (and thus freeze) the entire configuration as
        s.try_into()
    }
}
