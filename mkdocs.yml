site_name: Alinex Rust Server
site_description: A basic Rust based server to be used for specific applications.
site_author: Alexander Schilling
copyright: "Copyright &copy; 2021 Alexander Schilling, info@alinex.de"
site_dir: target/mkdocs

nav:
    - Home:
          - README.md
          - todo.md
          - CHANGELOG.md
          - policy.md
    - Development:
          - dev/README.md
          - roadmap.md
          - dev/setup.md
    - Demo:
          - demo/README.md
    - User Guide:
          - user/README.md

theme:
    name: material
    icon:
      logo: material/code-tags
    favicon: https://assets.gitlab-static.net/uploads/-/system/project/avatar/7603385/rust-logo-mozilla.jpg
    language: en
    palette:
      scheme: slate
      primary: grey
      accent: dark orange
    font:
      text: Lato
      code: Roboto Mono
    features:
      - navigation.instant

repo_name: "alinex/rust-server"
repo_url: "https://gitlab.com/alinex/rust-server"
edit_uri: ""

extra:
    manifest: "manifest.webmanifest"
    social:
    - icon: material/gitlab
      link: https://gitlab.com/alinex
    - icon: material/github
      link: https://github.com/alinex
    - icon: material/book-open-variant
      link: https://alinex.gitlab.io
    - icon: material/home
      link: https://alinex.de

extra_css:
    - assets/extra.css

extra_javascript:
  - assets/extra.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

plugins:
  - search
  - with-pdf:
      cover_subtitle: Collection of methods for easy working with promise based asynchronous calls
      cover_logo: https://alinex.gitlab.io/assets/icons/async.png
      toc_level: 3
      output_path: rust-server.pdf

markdown_extensions:
      - extra
      - toc:
            permalink: true
      - pymdownx.caret
      - pymdownx.tilde
      - pymdownx.mark
      - admonition
      - pymdownx.details
      - codehilite:
            guess_lang: false
            linenums: false
      - pymdownx.tabbed
      - pymdownx.superfences
      - pymdownx.inlinehilite
      - pymdownx.betterem:
            smart_enable: all
      - pymdownx.emoji:
            emoji_index: !!python/name:materialx.emoji.twemoji
            emoji_generator: !!python/name:materialx.emoji.to_svg
      - pymdownx.keys
      - pymdownx.smartsymbols
      - pymdownx.tasklist:
            custom_checkbox: true
      - markdown_blockdiag:
            format: svg
      - markdown_include.include
      - pymdownx.arithmatex:
            generic: true
