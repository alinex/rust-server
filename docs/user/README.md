# Overview

!!! warning

    At the moment we don't have a user manual, as the product is not ready, yet.
    But we will bring something up here as soon as it reaches the finish state.

The usage here is not aimed to end users because this is a skeleton project which has to be fulfilled with some real actions... Only the authentication part may be kept and so this documentation should be also ready for end users.

## Configuration

The server can be configured through 4 levels:

- Command Line Argume.nts
- Environment Variables
- Configuration files
- Using default values

As an example the following are the same:

- `alinex-server --webroot www` using argument
- environment setting `ALINEX_WEBROOT=www`
- config/default.toml: `webroot="www"`
- or using the default, which is also "www"

The value structures are megred bottom to top. First the files will be read:
- `default.*` can be of format `toml`, `yaml`, `json` or `ini`
- profile configuration, which can be specified using `--config ...` or as `ALINEX_CONFIG` environment.
- `local.*` which should never be in GIT and can change specific for individual host

The next part is merging the environment settings which can include everything which is also defineable in the file. 
See the default file contents. Each setting has to be prefixed with `ALINEX_` and use mor `_` as srtructure separator (`ALINEX_SERVER_IP` will set `server.ip` setting).

The program arguments contain not all settings but only some, which can be displayed using `--help` argument.

## Shell Completion

To install code completions generate the script using:

```bash
rust-server completions <SHELL>
```

Possible shells are: bash, fish or zsh.
