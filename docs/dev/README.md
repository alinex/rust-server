# Overview

This chapter describes the development of the application. This is intended for programmer helping to write or adapt the code for special needs.

## Tooling

For easy handling the common tasks are implemented as shell scripts:
- `bin/update` - update development environment
- `bin/dev` - to run development version
- `bin/test` - to check if everything works 
- `bin/release` - release new version
