# Setup

To setup the development environment you need the following.

## Rust compiler and tools

```bash
# initial install
curl https://sh.rustup.rs -sSf | sh && source $HOME/.cargo/env
sudo apt install pkg-config libssl-dev cmake
rustup component add rls-preview rust-analysis rust-src rustfmt-preview
rustup component add clippy-preview --toolchain nightly
# or update
rustup update
```

And the cargo sub commands and tools:

```bash
cargo +nightly install racer --force
cargo install rustsym --force
cargo install cargo-update --force
cargo install cargo-outdated --force
cargo install cargo-watch --force
```

## Clone repository

```bash
git clone https://gitlab.com/alinex/rust-server.git
```

## Development

To start the development process you should open a console within the project folder. You may start two automatic processes:

```bash
mkdocs serve & cargo watch -x run
```

This will start the documentation server on port 8000 and the real server on port 7000 and will restart/refresh if the source changed, automatically.
