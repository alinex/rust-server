# To Do List

This page is a list of tasks which need to be done. It is an easy and simple list to help me organize this project at first. Later I may change to a issue tracker or ticket system.

## Milestone 1 - Basic Web Server

-   [ ] configure
    -   [ ] parameters for port
    -   [ ] parameter ip address
    -   [ ] use ip address and port
-   [ ] logging
    -   [ ] log to stdout
    -   [ ] write access log
    -   [ ] configure path
    -   [ ] configure log level
-   [ ] test module
    -   [ ] simple text
    -   [ ] with parameters
    -   [ ] static
    -   [ ] vsite
    -   [ ] redirect
    -   [ ] async handler
    -   [ ] load on demand

## Milestone 2

## Later

-   [ ] database queries
-   [ ] authentication
    -   [ ] JWT
-   [ ] websocket

-   [ ]
-   [ ]
-   [ ]
-   [ ]
-   [ ]
-   [ ]
