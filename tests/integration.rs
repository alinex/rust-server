extern crate assert_cli;

#[cfg(test)]
mod integration {

    use assert_cli;

    #[test]
    fn calling() {
        assert_cli::Assert::main_binary()
            .stdout().contains("O B J E C T I X :   O B J E C T   B R O W S E R")
            .unwrap();
    }

    #[test]
    fn calling_help() {
        assert_cli::Assert::main_binary()
            .with_args(&["-h"])
            .stdout().contains("USAGE:")
            .unwrap();
    }

    #[test]
    fn calling_version() {
        assert_cli::Assert::main_binary()
            .with_args(&["-V"])
            .stdout().contains("objectix")
            .unwrap();
    }
}
