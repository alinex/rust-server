# Rust Server

A basic Rust based server to be used for specific applications.

The Rust Server is an approach to make a more robust and better performing server than the already used [REST Server](https://gitlab.com/alinex/node-server) written in NodeJS.

Please have a look at the full documentation containing further information and usage examples under [alinex.gitlab.io/rust-server](https://alinex.gitlab.io/rust-server).

_Alexander Schilling_
