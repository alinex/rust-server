use config;
use failure::Error;
use std::collections::HashMap;

// struct Context {
//     t: i32,
// }
//
// thread_local! {
//     static CONTEXTS: Context = Context {t: 99};
// }
//
// pub fn with_context<F: FnMut(&Context)>(f: F) {
//     CONTEXTS.with(f);
// }
//
// fn main() {
//     with_context(|c| println!("{}", c.t));
// }

pub fn init() -> Result<(), Error> {
  let mut settings = config::Config::default();
  settings
          // Add in `./config.toml`
          .merge(config::File::with_name("config")).unwrap()
          // Add in settings from the environment (with a prefix of APP)
          // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
          .merge(config::Environment::with_prefix("APP")).unwrap();

  // Print out our settings (as a HashMap)
  println!(
    "{:?}",
    settings.try_into::<HashMap<String, String>>().unwrap()
  );
  Ok(())
}
