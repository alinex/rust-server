#[macro_use]
extern crate clap;
extern crate console;
extern crate failure;
extern crate env_logger;

use std::io;
use std::net::ToSocketAddrs;
mod logo;
mod server;

use console::style;

/// Calling `run` and hadling global errors from it
fn main() {
    if let Err(ref e) = run() {
        eprintln!("Error: {}", style(pretty_error(e)).red().bold());
        if let Some(bt) = e.as_fail().backtrace() {
            eprintln!("{}", bt)
        }
        println!();
        std::process::exit(1);
    }
}

/// Command line specification
pub fn cli() -> clap::App<'static, 'static> {
    // validators
    let _is_int = |input: String| {
        if input.parse::<i32>().is_ok() {
            return Ok(());
        }
        Err(String::from("Argument isn't an integer"))
    };
    let is_address = |input: String| {
        if input.to_socket_addrs().is_ok() {
            return Ok(());
        }
        Err(String::from("Argument isn't a valid server address"))
    };
    // cli definition
    clap::App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .author(crate_authors!())
        .arg(
            clap::Arg::with_name("quiet")
                .short("q")
                .long("quiet")
                .global(true)
                .help("Output nothing unless really needed"),
        )
        .subcommand(
            clap::SubCommand::with_name("run")
                .about("Start web server")
                .arg(
                    clap::Arg::with_name("test")
                        .short("t")
                        .long("test")
                        .help("Start with test application"),
                )
                .arg(
                    clap::Arg::with_name("address")
                        .takes_value(true)
                        .default_value("127.0.0.1:3000")
                        .validator(is_address)
                        .help("The server address to listen to"),
                ),
        )
        .subcommand(
            clap::SubCommand::with_name("completions")
                .about("Generates completion scripts for your shell")
                .setting(clap::AppSettings::Hidden) // hide in help page
                .arg(
                    clap::Arg::with_name("SHELL")
                        .required(true)
                        .possible_values(&["bash", "fish", "zsh"])
                        .help("The shell to generate the script for"),
                ),
        )
}

/// Start the server
fn run() -> Result<(), failure::Error> {
    let args = cli().get_matches();
    let quiet = args.is_present("quiet");
    // Sub Commands
    match args.subcommand() {
        ("run", Some(sub_args)) => {
            // ouput header
            if !quiet {
                logo::print("Basic Rust Server");
                println!("Version       {}", crate_version!());
                println!("Author        {}", crate_authors!());
                println!();
            }
            // init logger
            std::env::set_var("RUST_LOG", "actix_web=info");
            env_logger::init();
            // start server
            let address = sub_args
                .value_of("address")
                .unwrap()
                .to_socket_addrs()
                .unwrap()
                .next()
                .unwrap();
            server::start(server::ServerSettings { 
                address, 
                quiet, 
                test: args.is_present("test") })?;
        }
        ("completions", Some(sub_args)) => {
            let shell = sub_args.value_of("SHELL").unwrap();
            cli().gen_completions_to(
                crate_name!(),
                shell.parse::<clap::Shell>().unwrap(),
                &mut io::stdout(),
            );
        }
        (_, _) => {
            return Err(failure::err_msg("Please give a command like `run` to execute."))
        }
    }
    Ok(())
}

/// Format error message with possible root cause as text string
fn pretty_error(err: &failure::Error) -> String {
    let mut pretty = err.to_string();
    let mut prev = err.as_fail();
    while let Some(next) = prev.cause() {
        pretty.push_str(": ");
        pretty.push_str(&next.to_string());
        prev = next;
    }
    pretty
}
