//! Control web server process

use actix;
use actix_web::{server, App, HttpRequest, Responder, fs};
use failure::{Error, ResultExt};
use std::net::SocketAddr;

mod test;

pub struct ServerSettings {
    pub address: SocketAddr,
    pub quiet: bool,
    pub test: bool,
}

/// start the server
pub fn start(config: ServerSettings) -> Result<(), Error> {
    let sys = actix::System::new("server");

    if !config.quiet {
        println!("Address       http://{}", config.address);
        println!();
        test::info();
    }
    server::new(|| {
        vec![            
            test::app_virtual(),
            test::app(),
            App::new()
                .resource("/", |r| r.f(greet))
                .resource("/favicon.ico", |r| r.f(favicon))
        ]
    })
    .bind(config.address)
    .context(format!("Could not bind {}", config.address))?
    .shutdown_timeout(60) // Timeout for graceful workers shutdown. (default 30 seconds)
    .start();
    println!();

    if !config.quiet {
        println!("Started HTTP server...");
    }
    let _ = sys.run(); // spawn new process with server
    if !config.quiet {
        println!("Stopped HTTP server.");
    }
    Ok(())
}

/// welcome message
fn greet(_req: &HttpRequest) -> impl Responder {
    format!("Welcome to Rust Server!")
}

/// favicon handler
fn favicon(_req: &HttpRequest) -> Result<fs::NamedFile, Error> {
    let file = "static/favicon.ico";
    Ok(fs::NamedFile::open(file).context(format!("Could not read file {}", file))?)
}
