//! # Alinex Objectix
//!
//! Server providing access to objects of different type. Mostly to analyze data structures bit it can also show processes or systems.
//!
//! ## Status
//!
//! At the moment this is an early development in which I mostly learn the language.

extern crate actix;
extern crate actix_web;
extern crate config;
extern crate console;
extern crate failure;

pub mod logo;
pub mod server;
pub mod settings;
